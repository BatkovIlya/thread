package ru.demo.muzon;

import java.io.*;

public class MuzonDownload extends Thread {

    private static final String OUT_FILE_TXT = "src/ru/demo/muzon/downloadmusic/outFile.txt";
    private static final String PATH_TO_MUSIC = "src/ru/demo/muzon/downloadmusic/music/music";


    @Override
    public void run() {
        try (BufferedReader musicFile = new BufferedReader(new FileReader(OUT_FILE_TXT))) {

            String music;

            try {
                for (int count = 0;(music = musicFile.readLine()) != null; count++) {

                    new Download(music, PATH_TO_MUSIC + String.valueOf(count) + ".mp3").start();

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
