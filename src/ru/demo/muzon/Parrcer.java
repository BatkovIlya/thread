package ru.demo.muzon;

import java.io.*;
import java.net.URL;
import java.util.Map;
import java.util.regex.*;
import java.util.stream.Collectors;


public class Parrcer extends Thread {
    private Pattern pattern;
    private BufferedReader bufferedReader;

    private static volatile BufferedWriter outFile;

    private Parrcer(Pattern pattern, BufferedReader bufferedReader) {
        this.pattern = pattern;
        this.bufferedReader = bufferedReader;
    }

    @Override
    public void run() {
        String result;

        while ((result = bufferedReader.lines().collect(Collectors.joining("\n"))) != null) {

            Pattern email_pattern = pattern;
            Matcher matcher = email_pattern.matcher(result);

            while (matcher.find()) {

                try {
                    write(matcher.group());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            break;

        }

    }


    private static synchronized void write(String string) throws IOException {
        outFile.write(string + "\r\n");
    }


    static void parrseout(Map<String, String> pathAndURL) throws IOException, InterruptedException {

        outFile = new BufferedWriter(new FileWriter("src/ru/demo/muzon/downloadmusic/outFile.txt"));

        Parrcer[] patternOuts = createdAndStart(/**pathAndURL.keySet().toArray(new String[0]),*/ pathAndURL);

        aliveAndJoin(patternOuts);

        outFile.close();

    }

    /**
     *
     * @return
     */

    private static Parrcer[] createdAndStart(Map<String, String> pathAndURL) {
        String[] key = pathAndURL.keySet().toArray(new String[0]);
        Parrcer[] patternOuts = new Parrcer[key.length];

        for (int i = 0; i < key.length; i++) {

            try {
                patternOuts[i] = new Parrcer(Pattern.compile(pathAndURL.get(key[i])), new BufferedReader(new InputStreamReader(new URL(key[i]).openStream())));
            } catch (IOException e) {
                e.printStackTrace();
            }

            patternOuts[i].start();
        }
        return patternOuts;
    }


    private static void aliveAndJoin(Parrcer[] patternOuts) throws InterruptedException {

        for (Parrcer patternOut : patternOuts) {
            if (patternOut.isAlive()) {
                patternOut.join();
            }
        }
    }
}