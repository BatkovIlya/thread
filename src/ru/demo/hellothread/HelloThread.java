package ru.demo.hellothread;

/**
 * Второй способ реализации Многопоточности
 */
public class HelloThread extends Thread {
    public void run() {
        for (int i = 4; i > 0; i--) {
            try {
                Thread.sleep(1000);
                if (i == 1) {
                    System.out.println("Hello from a thread");
                    System.out.println("Поток 1 завершен");
                }
            } catch (InterruptedException e) {
                System.out.println("Поток завешен");
            }
        }
    }

    public static void main(String[] args) {
        (new HelloThread()).start();
        for (int i = 2; i > 0; i--) {
            try {
                Thread.sleep(1000);
                if (i == 1) {
                    System.out.println("Hello from Mainer thread");
                    System.out.println("Поток 2 завешен");
                }
            } catch (InterruptedException e) {
                System.out.println("Поток завешен");
            }
        }
    }
}
