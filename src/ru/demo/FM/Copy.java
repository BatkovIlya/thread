package ru.demo.FM;

import java.io.*;

public class Copy extends Thread {
    private String nameInput;
    private String nameOutput;

    public Copy(String nameInput, String nameOutput) {
        this.nameInput = nameInput;
        this.nameOutput = nameOutput;
    }

    public void run() {
        long time = System.currentTimeMillis();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(nameInput));
             BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(nameOutput))) {
            String string;
            int i = 1;
            while ((string = bufferedReader.readLine()) != null) {
                bufferedWriter.write(string);
                i++;
            }
            time = time;
            System.out.println("хоба" +String.format("%,12d", time) + " nanosec");
        } catch (FileNotFoundException e) {

        } catch (IOException t) {

        }


    }

}
