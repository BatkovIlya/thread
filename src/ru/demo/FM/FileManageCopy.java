package ru.demo.FM;

public class FileManageCopy {

    public static final String NEW_TEXT_FILE = "src\\ru\\demo\\FM\\NewTextFile.txt";
    public static final String FIRST_COPY = "src\\ru\\demo\\FM\\FirstCopy.txt";
    public static final String SECOND_COPY = "src\\ru\\demo\\FM\\SecondCopy.txt";

    public static void main(String[] args) throws InterruptedException {
        Copy firstCopy = new Copy(NEW_TEXT_FILE, FIRST_COPY);
        Copy secondCopy = new Copy(NEW_TEXT_FILE, SECOND_COPY);
        firstCopy.start();
        secondCopy.start();
    }


}
