package ru.demo.CopyOnOne;


import java.io.*;

/**
 * Класс, в котором реализовано многопоточное считывание данных из файлов из запись в результирующий файл.
 *
 * @author Батьков Илья
 */

public class Main extends Thread {
    private static volatile BufferedWriter bufferedWriter;
    private String adress;

    public Main(String adress) {
        this.adress = adress;
    }

    public static void main(String[] args) throws IOException, InterruptedException {

        Main firstThread = new Main("first.txt");
        Main secondThread = new Main("second.txt");
        bufferedWriter = new BufferedWriter(new FileWriter("output.txt"));

        firstThread.start();
        secondThread.start();

        if (firstThread.isAlive()) {
            firstThread.join();
        }
        if (secondThread.isAlive()) {
            secondThread.join();
        }

        bufferedWriter.close();
    }

    public void run() {
        String string;
        try (BufferedReader first = new BufferedReader(new FileReader(adress)))
        {
            while ((string = first.readLine()) != null) {
                write(string + "\n");

            }
        } catch (IOException e) {
        }

    }

    /**
     * Метод для записи строки в результирующий файл - "output.txt"
     *
     * @param string строка, которую нужно записать в файл
     * @throws IOException исключение
     */

    public static synchronized void write(String string) throws IOException{
        bufferedWriter.write(string);
    }
}
