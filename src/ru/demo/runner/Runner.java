package ru.demo.runner;

public class Runner extends Thread {

    private int priority1;
    private int priority2;

    public Runner(String name, int priority1, int priority2) {
        super.setName(name);
        this.priority1 = priority1;
        this.priority2 = priority2;
    }

    public void run(){

        setPriority(priority1);
        for(int i=1; i<1001; i++){
            try{
                sleep(1);
            }catch (InterruptedException e){ }
            if(i==500){
                setPriority(priority2);
            }
            System.out.println(getName() + " пробежал "+ i);
        }
    }



}
