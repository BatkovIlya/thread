package ru.demo.hellorunnable;

/**
 * Реализвация интерфейса Runnable
 *
 * Интерфейс Runnable определяет один метод run,
 * предназначенный для размещение кода, исполняемого в потоке.
 * Runnable-обьект пересылается к конструктору Thread
 * и с помощью метода start() поток запускается.
 */
public class HelloRunnable implements Runnable {
    public void run() {

        /**
         * Данный поток выводит данные после 4 секунд после запуска программы
         * и через две секунды после главного потока
         */
        for (int i=4;1>0;i--){
            try{
                Thread.sleep(1000);
                if (i==1){
                    System.out.println("Hello from a thread");
                    System.out.println("Поток 1 завершен");
                }
            }catch (InterruptedException e){
                System.out.println("Поток завешен");
            }
        }
    }

    /**
     * Данный поток выведет информацию после 2 секунд с запуска программы и на две секунды раньше, чем первый поток
     * @param args
     */
    public static void main(String[] args) {
        (new Thread(new HelloRunnable())).start();

        for (int i=2;1>0;i--){
            try{
                Thread.sleep(1000);
                if (i==1){
                    System.out.println("Hello from Main thread");
                    System.out.println("Поток 2 завешен");
                }
            }catch (InterruptedException e){
                System.out.println("Поток завешен");
            }
        }
    }
}

