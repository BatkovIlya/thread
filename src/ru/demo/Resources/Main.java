package ru.demo.Resources;

/**
 * Запуск программы, которая инкременирует переменную i до двух тысяч с помощью двух потоков.
 * @author ITA
 */

public class Main {
    public static void main(String[] args) throws InterruptedException {
        new InterferenceExample().example();
    }
}
